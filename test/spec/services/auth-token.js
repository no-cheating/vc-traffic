'use strict';

describe('Service: authToken', function () {

  // load the service's module
  beforeEach(module('trafficApp'));

  // instantiate service
  var authToken;
  beforeEach(inject(function (_authToken_) {
    authToken = _authToken_;
  }));

  it('should start with a no token', function () {
    expect(authToken.getToken()).toBe(null);
  });

  it('should accept and remember a token', function() {
    authToken.setToken('password');
    expect(authToken.getToken()).toBe('password');
  })

  it('should have an empty token after deleting', function() {
    authToken.deleteToken();
    expect(authToken.getToken()).toBe(null);
  })

});
