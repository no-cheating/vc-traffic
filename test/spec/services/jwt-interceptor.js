'use strict';

describe('Service: jwtInterceptor', function () {

  // load the service's module
  beforeEach(module('trafficApp'));

  // instantiate service
  var jwtInterceptor;
  beforeEach(inject(function (_jwtInterceptor_) {
    jwtInterceptor = _jwtInterceptor_;
  }));

  it('should do something', function () {
    expect(!!jwtInterceptor).toBe(true);
  });

});
