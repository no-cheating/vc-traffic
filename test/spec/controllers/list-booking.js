'use strict';

describe('Controller: ListBookingCtrl', function () {

  // load the controller's module
  beforeEach(module('trafficApp'));

  var ListBookingCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListBookingCtrl = $controller('ListBookingCtrl', {
      $scope: scope,
      bookings: []
    });

  }));

  it('should be truthy', function () {
    expect(!!ListBookingCtrl).toBe(true);
  });
});
