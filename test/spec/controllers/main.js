'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('trafficApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should be truthy', function () {
    expect(!!scope).toBe(true);
  });
});
