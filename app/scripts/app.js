'use strict';

/**
 * @ngdoc overview
 * @name trafficApp
 * @description
 * # trafficApp
 *
 * Main module of the application.
 */
angular
  .module('trafficApp', [
    'constants',
    'ngTouch',
    'ui.router',
    'ngLodash',
    'ngResource',
    'http-auth-interceptor',
    'ngMessages',
    'ui.bootstrap',
    'angularSpinner',
    'ngTable',
    'ngToast'
  ])

  // Don't strip trailing slashes from Resource URLs
  .config(['$resourceProvider', function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
  }])

  .config(['$httpProvider', function ($httpProvider) {

    // Add the JSON Web Token http request interceptor
    $httpProvider.interceptors.push('jwtInterceptor');

    // Add HTTP response status interceptor
    $httpProvider.interceptors.push('httpStatusInterceptor');

  }])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('list', {
        url: '/list',
        controller: 'ListBookingCtrl',
        templateUrl: 'views/list-booking.html'
      })
      .state('day', {
        url: '/day/{day}',
        controller: 'DayBookingCtrl',
        templateUrl: 'views/day-booking.html',
        resolve: {
          bookings: function(Booking, $stateParams) {
            if ($stateParams.day) {
              return Booking.query({
                startDate: $stateParams.day,
                endDate: $stateParams.day
              }).$promise;
            }
          }
        }
      })
      .state('month', {
        url: '/month/{month}',
        controller: 'MonthBookingCtrl',
        templateUrl: 'views/month-booking.html',
        resolve: {
          bookings: function(Booking, $stateParams, $filter) {
            if ($stateParams.month) {
              var date = new Date($stateParams.month),
                start = $filter('date')(date, 'yyyy-MM-dd');
              date.setMonth(date.getMonth() + 1);
              date.setDate(date.getDate() - 1);
              return Booking.summary({
                startDate: start,
                endDate: $filter('date')(date, 'yyyy-MM-dd')
              }).$promise;
            }
          }
        }
      })

    $urlRouterProvider.otherwise('day/');
  });
