'use strict';

/**
 * @ngdoc service
 * @name trafficApp.trafficReport
 * @description
 * # trafficReport
 * Service in the trafficApp.
 */
angular.module('trafficApp')
  .service('TrafficReport', function ($resource, urls) {

    var TrafficReport = $resource(urls.trafficReport, {}, {
      create: {
        method: 'POST'
      }
    });

    TrafficReport.TYPE_REFUSED = 'Refused';
    TrafficReport.TYPE_DECLINED = 'Declined';
    TrafficReport.TYPE_CANCELLED = 'Cancelled';
    TrafficReport.TYPE_REPORTED = 'Reported';

    return TrafficReport;

  });
