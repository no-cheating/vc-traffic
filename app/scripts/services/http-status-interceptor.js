'use strict';

/**
 * @ngdoc service
 * @name errorInterceptor
 * @description
 * # errorInterceptor
 * HTTP interceptor for unsuccessful http responses.
 */
angular.module('trafficApp')
  .factory('httpStatusInterceptor', function ($rootScope, $q) {
    return {

      responseError: function (rejection) {

        if (0 === rejection.status) {
          $rootScope.$broadcast('event:http-server-absent', rejection);
        } else {
          $rootScope.$broadcast('event:http-server-present', rejection);
          switch (rejection.status) {
            case 400:
              $rootScope.$broadcast('event:http-bad-request', rejection);
              break;
            case 500:
              $rootScope.$broadcast('event:http-server-error', rejection);
              break;
          }
        }

        // Carry on with the rejection
        return $q.reject(rejection);
      }
    };
  });
