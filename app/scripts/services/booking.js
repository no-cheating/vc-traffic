'use strict';

/**
 * @ngdoc service
 * @name trafficApp.Booking
 * @description
 * # Booking
 * Resource for accessing booking data
 */
angular.module('trafficApp')
  .factory('Booking', function ($resource, urls, lodash, TrafficStatus, $http, venueSettings) {

    var Booking = $resource(urls.trafficData + ':id/', {
      id: '@id'
    }, {
      query: {
        method: 'GET',
        isArray: true,
        transformResponse: transformSuccess(function(data) {
          return lodash.map(data, annotateBookingFull);
        })
      },
      get: {
        method: 'GET',
        transformResponse: transformSuccess(annotateBookingFull)
      },
      summary: {
        method: 'GET',
        isArray: true,
        url: urls.trafficSummary,
        transformResponse: transformSuccess(function(data) {
          return lodash.map(data, annotateBookingBasic);
        })
      }
    });

    var REQUEST_TYPES = {
      'porter': 'Porter',
      'goodsLift': 'Goods Lift',
      'forklift': 'Forklift'
    };

    Booking.SOURCE_WEBSITE = 'a';
    Booking.SOURCE_QUICK = 't';

    /**
     * Get the current status code from a collection of statusses.
     * @param statusses
     * @returns {int}
     */
    function getCurrentStatus(booking) {
      if (booking.statusses.length) {
        // Return the highest status code.
        return lodash.max(lodash.pluck(booking.statusses, 'status'));
      } else {
        if ( venueSettings.bookings.approvalRequired && ! booking.approval ) {
          // Booking has no recorded status, and is not approved.
          return TrafficStatus.STATUS_UNSCHEDULED;
        } else {
          // Booking has an Approval or the Venue does not require that.
          return TrafficStatus.STATUS_SCHEDULED;
        }
      }
    }

    function annotateBookingFull(booking) {

      // Annotate the booking with the highest of its statusses.
      booking.status = getCurrentStatus(booking);

      // Make a JS Date for the booking scheduled arrival time
      booking.scheduledTimeIn = Booking.localiseUTCDate(booking.scheduledTimeIn);
      booking.scheduledTimeOut = Booking.localiseUTCDate(booking.scheduledTimeOut);

      //@TODO refactor the following four non-DRY code blocks

      // Try to find a checkin status, and store store it as the checkin status
      booking.checkinStatus = lodash.find(booking.statusses, {status: TrafficStatus.STATUS_CHECKED_IN});
      if (booking.checkinStatus) {
        // We convert to local time
        booking.timeIn = new Date(booking.checkinStatus.whenRecorded);
      }

      // Try to find a checkin status, and store store it as the checkin status
      booking.checkoutStatus = lodash.find(booking.statusses, {status: TrafficStatus.STATUS_CHECKED_OUT});
      if (booking.checkoutStatus) {
        booking.timeOut = new Date(booking.checkoutStatus.whenRecorded);
      }

      // Try to find a status for a refusal
      booking.refusalStatus = lodash.find(booking.statusses, {status: TrafficStatus.STATUS_REFUSED});
      if (booking.refusalStatus) {
        booking.timeRefused = new Date(booking.refusalStatus.whenRecorded);
      }

      // Try to find a status for a declining
      booking.declinedStatus = lodash.find(booking.statusses, {status: TrafficStatus.STATUS_DECLINED});
      if (booking.declinedStatus) {
        booking.timeDeclined = new Date(booking.declinedStatus.whenRecorded);
      }

      // Text of the special requests
      booking.requests = lodash.sortBy(lodash.values(lodash.filter(REQUEST_TYPES, function(label, key) {
        return booking[key];
      }))).join(', ');

      // Change the word 'Rejected' to 'Refused' in any reports.
      lodash.each(booking.reports, function(report) {
        if ('Rejected' === report.type) {
          report.type = 'Refused';
        }
      });

      return booking;
    }

    /**
     * Compute additional information about a Booking.
     * @param booking
     * @returns {*}
     */
    function annotateBookingBasic(booking) {

      if ( ! venueSettings.bookings.approvalRequired && booking.status === TrafficStatus.STATUS_UNSCHEDULED ) {
        booking.status = TrafficStatus.STATUS_SCHEDULED;
      }

      return booking;
    }

    /**
     * The server stores 'datetimes' as dates plus times.  They're meant to be 'local' times, so we adjust relative
     * to UTC here.  If a falsey argument is passed, null is returned.
     * Date interpreted as UTC.
     * @param {Date} date ({Date} or anything passable to Date constructor).
     * @returns {Date|null}
     */
    Booking.localiseUTCDate = function (date) {
      if ( ! date ) {
        return null;
      }

      var date = new Date(date);
      date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
      return date;
    }

    /**
     * Add a callback to a successful http response.
     * @param {Function} cb
     */
    function transformSuccess(cb) {
      return $http.defaults.transformResponse.concat(function (data, getHeaders, statusCode) {
        if (200 !== statusCode) {
          return data;
        }
        return cb(data);
      });
    }

    /**
     * Predicate saying that the booking intersects another booking.
     * @param otherInterval
     * @returns {boolean}
     */
    Booking.prototype.intersects = function(otherBooking) {
      return this.scheduledTimeOut.getTime() > otherBooking.scheduledTimeIn.getTime() &&
        this.scheduledTimeIn.getTime() < otherBooking.scheduledTimeOut.getTime();
    };

    Booking.prototype.scheduledTimeOut = function() {
      if (this.scheduledTimeIn && this.loadTimeMod) {
        var date = new Date(this.scheduledTimeIn);
        date.setMinutes(date.getMinutes() + (30 * this.loadTimeMod));
        return date;
      }
    };

    /**
     * A css class for the booking's status.
     * @returns {string}
     */
    Booking.prototype.statusClass = function() {
      return 'booking-status-' + slugify(TrafficStatus.statusText(this.status));
    };

    return Booking;

    /**
     * A lower-case version of the text, with hyphens replacing spaces.
     * @param text
     * @returns {string}
     */
    function slugify(text) {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '');
    }

  });
