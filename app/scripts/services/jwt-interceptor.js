'use strict';

/**
 * @ngdoc service
 * @name trafficApp.jwtInterceptor
 * @description
 * # jwtInterceptor
 * Service in the trafficApp.
 */
angular.module('trafficApp')
  .factory('jwtInterceptor', function ($q, authToken) {
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if (authToken.getToken()) {
          config.headers.Authorization = 'JWT ' + authToken.getToken();
        }
        return config;
      }
    };
  });
