'use strict';

/**
 * @ngdoc service
 * @name trafficApp.trafficStatus
 * @description
 * # trafficStatus
 * Service in the trafficApp.
 * Resource corresponding to a TrafficStatus model.
 */
angular.module('trafficApp')
  .factory('TrafficStatus', function ($resource, urls) {

    var TrafficStatus = $resource(urls.trafficStatus, {
      'id': '@id'
    }, {
      create: {
        method: 'POST'
      },
      delete: {
        method: 'DELETE',
        url: urls.trafficStatus + ':id/'
      }
    });

    TrafficStatus.STATUS_UNSCHEDULED = -1;
    TrafficStatus.STATUS_SCHEDULED = 0;
    TrafficStatus.STATUS_CHECKED_IN = 1;
    TrafficStatus.STATUS_CHECKED_OUT = 2;
    TrafficStatus.STATUS_REFUSED = 3;
    TrafficStatus.STATUS_DECLINED = 4;
    TrafficStatus.STATUS_CANCELLED = 5;

    TrafficStatus.statusText = function(statusID) {
      switch (statusID) {
        case TrafficStatus.STATUS_CHECKED_IN:
          return 'Checked in';
        case TrafficStatus.STATUS_CHECKED_OUT:
          return 'Checked out';
        case TrafficStatus.STATUS_DECLINED:
          return 'Declined';
        case TrafficStatus.STATUS_REFUSED:
          return 'Refused';
        case TrafficStatus.STATUS_SCHEDULED:
          return 'Scheduled';
        case TrafficStatus.STATUS_UNSCHEDULED:
          return 'For approval';
        case TrafficStatus.STATUS_CANCELLED:
          return 'Cancelled';
      }
    }
    TrafficStatus.STATUS_SCHEDULED = 0;
    TrafficStatus.STATUS_CHECKED_IN = 1;
    TrafficStatus.STATUS_CHECKED_OUT = 2;
    TrafficStatus.STATUS_REFUSED = 3;
    TrafficStatus.STATUS_DECLINED = 4;


    /**
     * Make a TrafficStatus for the given Booking and with the given properties.
     * @param {Booking} booking
     * @param {object} properties required key: status; optional: reason.
     * @returns {TrafficStatus}
     */
    function makeForBooking(booking, properties) {
      return new TrafficStatus(angular.extend({
        vehicleBooking: booking.bookingVehicle,
        journey: booking.journey
      }, properties));
    }

    /**
     * Create a traffic status with the given properties for the given booking, and save it to the server.
     * @param {Booking} booking
     * @param {object} properties required key: status; optional: reason.
     * @returns {TrafficStatus} saved resource.
     */
    TrafficStatus.createForBooking = function(booking, properties) {
      if ([
          TrafficStatus.STATUS_CHECKED_IN,
          TrafficStatus.STATUS_CHECKED_OUT,
          TrafficStatus.STATUS_REFUSED,
          TrafficStatus.STATUS_DECLINED,
          TrafficStatus.STATUS_CANCELLED
        ].indexOf(properties.status) === -1) {
        // Not a valid (savable) status.
        throw new Error('Invalid traffic status: ' + properties.status);
      }
      return makeForBooking(booking, properties).$create();
    };

    return TrafficStatus;

  });
