'use strict';

/**
 * @ngdoc service
 * @name trafficApp.urls
 * @description
 * # urls
 * URLs for AJAX requests.
 */
angular.module('trafficApp')
  .service('urls', function (baseUrl) {

    // Login
    this.login = baseUrl + 'jwt/login/';

    // Verify JWT
    this.loggedIn = baseUrl + 'jwt/verify/'

    // Booking data (full)
    this.trafficData = baseUrl + 'traffic/booking-data/';
    // Booking data (summary)
    this.trafficSummary = baseUrl + 'traffic/booking-summary/';

    // Traffic status
    this.trafficStatus = baseUrl + 'traffic/status/';

    // Traffic status
    this.trafficReport = baseUrl + 'traffic/report/';

    // Booking approval
    this.bookingApproval = baseUrl + 'traffic/approval/:id/';

    this.checkStatus = baseUrl + 'traffic/';

  });
