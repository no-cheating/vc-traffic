'use strict';

/**
 * @ngdoc service
 * @name trafficApp.bookingApproval
 * @description
 * # bookingApproval
 * Service in the trafficApp.
 */
angular.module('trafficApp')
  .factory('BookingApproval', function ($resource, urls) {

    return new $resource(urls.bookingApproval, {
      id: '@id'
    }, {
      create: {
        method: 'POST'
      },
      delete: {
        method: 'DELETE'
      }
    })

  });
