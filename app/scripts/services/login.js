'use strict';

/**
 * @ngdoc service
 * @name attendanceApp.login
 * @description
 * # login
 * Service for handling logins.
 */
angular.module('trafficApp')
  .service('login', function ($q, $http, authService, authToken, urls, $window, $state) {

    /**
     * Attempts to log in using the given credentials.
     * @param user
     * @returns {promise}
     */
    this.login = function (user) {
      var deferred = $q.defer();

      $http
        .post(urls.login, user).
        success(function (data) {
          // Store the token
          authToken.setToken(data.token);
          // Call the auth service's loginConfirmed message, which will trigger any postponed (intercepted) http
          // requests, and broadcast event:auth-loginConfirmed.
          authService.loginConfirmed();

          deferred.resolve();
        }).
        error(function (data, status) {
          // 'Forget' the token if the user fails to log in
          authToken.deleteToken();
          // The request has failed, either because of bad credentials or some other server problem.
          // We'll determine what the problem was by inspecting the response's status code.
          var messages = {};
          if (400 == status) {
            messages.badCredentials = true;
          } else {
            messages.serverError = true;
          }
          deferred.reject(messages);
        });

      return deferred.promise;

    };

    /**
     * Checks whether the user has a valid Json Web Token.
     * @return {Promise} resolving to boolean
     */
    this.checkLogin = function () {
      var deferred = $q.defer();
      if ( ! authToken.getToken() ) {
        deferred.resolve(false);
      } else {
        $http.post(urls.loggedIn, {token: authToken.getToken()})
          .success(function(data) {
            deferred.resolve(true);
          })
          .error(function(data) {
            deferred.resolve(false);
          })
      }
      return deferred.promise;
    };

    /**
     * Log out any user.
     */
    this.logout = function() {
      authToken.deleteToken();
    };

  });
