'use strict';

/**
 * @ngdoc service
 * @name trafficApp.auth
 * @description
 * # auth
 * Service in the trafficApp.
 */
angular.module('trafficApp')
  .service('authToken', function ($window) {
    var TOKEN_KEY = 'json-web-token';

    this.getToken = getToken;
    this.setToken = setToken;
    this.deleteToken = deleteToken;

    /**
     * Get the stored JSON web token.
     * @returns {string}
     */
    function getToken() {
      return $window.localStorage.getItem(TOKEN_KEY);
    }

    /**
     * Set the JSON web token.
     * @param the JSON web-token, received from the server.
     */
    function setToken(value) {
      $window.localStorage.setItem(TOKEN_KEY, value);
    }

    /**
     * Log out any user.
     */
    function deleteToken() {
      $window.localStorage.removeItem(TOKEN_KEY);
    }

  });
