'use strict';
/**
 * @ngdoc function
 * @name trafficApp.controller:ListBookingsCtrl
 * @description
 * # ListBookingsCtrl
 * List of bookings over a date range.
 */
angular.module('trafficApp')
  .controller('ListBookingCtrl', function ($scope, Booking, $filter, ngTableParams, $modal, venueSettings, TrafficStatus) {

    var defaultStartDate = new Date(),
      defaultEndDate = new Date(defaultStartDate);
    defaultEndDate.setDate(defaultEndDate.getDate() + 6);

    $scope.approvalRequired = venueSettings.bookings.approvalRequired;

    $scope.$watch('dayStrings', function (dayStrings) {
      Booking.query(dayStrings).$promise.then(function (bookings) {
        $scope.allBookings = bookings;
      });
    }, true);

    /**
     * Set start- and end-dates, read from state parameters.
     */
    $scope.dayStrings = {
      startDate: $filter('date')(defaultStartDate, 'yyyy-MM-dd'),
      endDate: $filter('date')(defaultEndDate, 'yyyy-MM-dd')
    };

    // When visible bookings change, set up the table
    $scope.$watch('visibleBookings', function (bookings) {
      if (bookings) {
        $scope.tableParams = new ngTableParams({
          page: 1,
          count: 20
        }, {
          total: bookings.length,
          getData: function ($defer, params) {
            var orderedBookings = params.sorting() ? $filter('orderBy')(bookings, params.orderBy()) : bookings;
            $defer.resolve(orderedBookings.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
      }
    });

    // View detail of booking; show checking in and out buttons
    $scope.editBooking = function (booking) {
      $modal.open({
        controller: 'DetailBookingCtrl',
        templateUrl: 'views/detail-booking.html',
        resolve: {
          booking: function () {
            return booking;
          }
        }
      });
    };

    $scope.statusText = TrafficStatus.statusText;

  });
