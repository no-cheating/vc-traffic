'use strict';

/**
 * @ngdoc function
 * @name trafficApp.controller:DayBookingCtrl
 * @description
 * # DayBookingCtrl
 * Single day of bookings.
 */
angular.module('trafficApp')
  .controller('DayBookingCtrl', function ($scope, Booking, bookings, $filter, lodash, $modal, $stateParams, $state, $timeout, $window) {

    var date;

    /**
     * Name of the booking field used to display and sort.
     * @type {string}
     */
    $scope.displayField = 'vrn';

    $scope.dayString = $stateParams.day || formatDate(new Date());

    $scope.$watch('dayString', function(day) {
      $state.go('day', {
        day: day
      });
    }, true);

    date = Booking.localiseUTCDate($scope.dayString);

    if (date) {
      $scope.startDate = new Date(date);
      date.setDate($scope.startDate.getDate() + 1);
      $scope.endDate = date;
      $scope.allBookings = bookings;
    }

    // Position the time line, and re-position it every 30 seconds.
    updateTimeLine();
    $timeout(updateTimeLine, 30000);

    $scope.$watch('visibleBookings', positionBookings);

    $scope.$watch('displayField', positionBookings);

    $scope.$watch('startDate', setTimeSpan);

    $scope.$watch('endDate', setTimeSpan);

    /**
     * @TODO DRY this code (repeated in ListBookingCtrl)
     * View the detail of a booking
     * @param booking
     */
    $scope.editBooking = function(booking) {
      $modal.open({
        controller: 'DetailBookingCtrl',
        templateUrl: 'views/detail-booking.html',
        resolve: {
          booking: function() {
            return booking;
          }
        }
      });
    };


    /**
     * Set the list of times to be displayed.
     * @return {Array[Date]} An ordered array of datetimes each one hour apart, spanning the date range.
     */
    function setTimeSpan() {
      if ($scope.startDate && $scope.endDate && $scope.startDate < $scope.endDate) {
        var times = [$scope.startDate],
          date;
        while (times[times.length - 1] < $scope.endDate) {
          date = new Date(times[times.length -1]);
          date.setHours(date.getHours() + 1);
          times.push(date);
        }
        // Lose the last element
        times.pop();
        $scope.times = times;
      }
    }

    function positionBookings() {

      var sortedBookings = lodash.sortByAll($scope.visibleBookings, ['scheduledTimeIn', upperCaser($scope.displayField)]);

      if ( ! ( sortedBookings && sortedBookings.length ) ) {
        $scope.bookingStack = null;
      } else {
        var minTime = $scope.startDate.getTime(),
          maxTime = $scope.endDate.getTime(),
          percentageMultiplier = 100 / (maxTime - minTime);

        $scope.left = function (date) {
          return (date.getTime() - minTime) * percentageMultiplier + '%';
        };

        $scope.width = function(stackedBooking) {
          return Math.round((stackedBooking.booking.scheduledTimeOut.getTime() - stackedBooking.booking.scheduledTimeIn.getTime()) * percentageMultiplier) + '%';
        };

        $scope.bookingStack = lodash.reduce(sortedBookings, function (stack, booking) {
          return stack.addToCollection(booking);
        }, new BookingStack());

      }

    }

    /**
     * Takes a field name and returns a function mapping anything to an upper-case string of its 'field' property.
     * @param field The field name
     * @returns {Function} mapping an item to an upper-case string of its value for 'field'.
     */
    function upperCaser(field) {
      return function(item) {
        return String(item[field]).toUpperCase();
      }
    }

    /**
     * A stack of intervals, so a collection of Intervals each one associated with a row.
     * @constructor
     */
    function BookingStack() {

      var bookingStack = this;

      this.items = [];

      /**
       * Find the lowest row at which an interval can fit in to this stack of intervals, add it at that row,
       * @param booking Booking
       * @return {BookingStack}
       * @chainable
       */
      this.addToCollection = function(booking) {

        var clashingBookingRows = lodash.pluck(lodash.filter(bookingStack.items, intersector(booking)), 'row');

        var lowestFreeRow = firstGap(clashingBookingRows);

        /**
         * Find the lowest number which is not present in the given array of natural numbers.
         * @param arrayOfNaturalNumbers array of natural numbers.
         * @returns {int}
         */
        function firstGap(arrayOfNaturalNumbers) {

          var orderedArrayOfNaturalNumbers = lodash.unique(lodash.sortBy(arrayOfNaturalNumbers)),
            gap = 0;

          while (orderedArrayOfNaturalNumbers[gap] === gap) {
            gap++;
          }

          return gap;
        }

        bookingStack.items.push({
          booking: booking,
          row: lowestFreeRow
        });

        function intersector(unstackedBooking) {
          return function(stackedBooking) {
            return stackedBooking.booking.intersects(unstackedBooking);
          }
        }

        return this;
      };

      /**
       * Get the array of intervals with their row.
       * @returns {Array} of {interval: {Interval}, row: {int}}
       */
      this.getRows = function() {
        return lodash.reduce(bookingStack.items, function(rows, stackedBooking) {
          rows[stackedBooking.row] = (rows[stackedBooking.row] || []).concat(stackedBooking.booking);
          return rows;
        }, []);
      };

    }

    /**
     * Formats a date
     * @param date
     * @returns {string}
     */
    function formatDate(date) {
      return $filter('date')(date, 'yyyy-MM-dd')
    }

    /**
     * Checks if the selected day is today, and if so calculates how far we are through the day.
     * Sets appropriate scope variables.
     */
    function updateTimeLine() {
      var now = new Date();
      console.log(now, $scope.dayString);
      $scope.isToday = $scope.dayString === formatDate(now);
      if ($scope.isToday) {
        $scope.percentThroughDay = (now.getHours() * 60 + now.getMinutes()) / 14.4 + '%';
      }
    }


  });
