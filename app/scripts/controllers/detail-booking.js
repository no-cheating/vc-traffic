'use strict';

/**
 * @ngdoc function
 * @name trafficApp.controller:DetailBookingCtrl
 * @description
 * # DetailBookingCtrl
 * Controller of the trafficApp
 */
angular.module('trafficApp')
  .controller('DetailBookingCtrl', function ($scope, booking, TrafficStatus, $http, BookingApproval, lodash, ngToast, $modal, $q, TrafficReport, venueSettings) {

    // List of possible reasons, to accompany a given TrafficStatus.
    var REASONS = [
      {
        status: TrafficStatus.STATUS_DECLINED,
        label: 'Select the reason for declining this booking',
        reasons: [
          'Recipient unavailable',
          'Inappropriate time',
          'Too late notice',
          'Other'
        ],
        reportType: TrafficReport.TYPE_DECLINED
      },
      {
        status: TrafficStatus.STATUS_REFUSED,
        label: 'Select the reason for refusing this booking',
        reasons: [
          'No High Vis Vest',
          'Too Late For Booked Slot',
          'Too Early For Booked Slot',
          'Abusive Driver',
          'Multiple Bookings Over Allowed Amount'
        ],
        reportType: TrafficReport.TYPE_REFUSED
      },
      {
        status: TrafficStatus.STATUS_CANCELLED,
        label: 'Select the reason for cancelling this booking',
        reasons: [
          'Incorrect booking',
          'No longer needed',
          'Venue issues',
          'Other'
        ],
        reportType: TrafficReport.TYPE_CANCELLED
      }
    ];

    var REPORT_REASONS = {
      label: 'Select the reason for reporting this booking',
      reasons: [
        'Verbal Abuse',
        'Physical Threat',
        'Overstayed Time Allocation'
      ],
      reportType: TrafficReport.TYPE_REPORTED
    };

    $scope.TrafficStatus = TrafficStatus;

    $scope.approvalRequired = venueSettings.bookings.approvalRequired;

    $scope.booking = booking;

    /**
     * Delete the given status.
     * @param {TrafficStatus} status
     */
    $scope.deleteStatus = function(status) {
      var status = new TrafficStatus(status);
      status.$delete().finally(reloadBooking);
    };

    /**
     * Save an approval for the given booking.
     * @param {Booking} bookingSlot
     */
    $scope.approve = function(booking) {
      BookingApproval.create({booking: booking.id}).$promise.finally(reloadBooking);
    };

    /**
     * Cancel the given approval, by ID.
     * @param id internal id of the given Approval.
     */
    $scope.cancelApproval = function(approval) {
      BookingApproval.delete(approval).$promise.finally(reloadBooking);
    };

    /**
     * Set the status of the scope's booking.
     * @param status
     * @returns {Function}
     */
    $scope.setStatus = function setStatus(status) {

      getReasonPromise().then(function(report) {
        TrafficStatus.createForBooking($scope.booking, {
          status: status,
          report: report
        }).finally(function () {
          booking.$get().finally(function () {
            $scope.isSaving = false;
          })
        });
      });

      /**
       * Makes a Promise resolving to additional information to accompany the status report, if any is needed.
       * For statusses other than refusal, no addition information is needed so in those cases the promise  resolves
       * immediately.  Otherwise a modal dialog is shown and the promise returns the result of that, or rejects if the
       * modal is cancelled.
       * @returns {$q.Promise}
       */
      function getReasonPromise() {
        var deferred = $q.defer(),
          reasons = lodash.find(REASONS, {status: status});

        if (reasons) {
          showReportModal(reasons).then(function(report) {
            // A reason was supplied
            deferred.resolve(report);
          }).catch(function() {
            // No reason given
            deferred.reject();
          });
        } else {
          // No reason needed for this status
          deferred.resolve();
        }
        return deferred.promise;
      }

    };

    /**
     * Reload the booking.
     */
    function reloadBooking() {
      $scope.booking.$get();
    }

    // When there's an http request in progress, the buttons are disabled.
    $scope.$watch(function () {
      return $scope.inProgress = $http.pendingRequests.length > 0;
    });

    $scope.$on('event:http-bad-request', function(event, response) {
      lodash.each(response.data, function(errors, key) {
        ngToast.danger(key + ': ' + errors.join(', '));
      });
    });

    $scope.reportBooking = function() {
      showReportModal(REPORT_REASONS).then(function(reportData) {
        var report = new TrafficReport(reportData);
        report.$create().finally(reloadBooking);
      });
    };

    /**
     * Shows a modal dialogue for specifying a 'reason', from a list, and optionally a comment.
     * @param {{label: string, reasons: Array}} reasonData The reasons to show in the modal.
     * @returns {Promise}
     */
    function showReportModal(reasonData) {

      var modal = $modal.open({
        controller: function ($scope) {

          $scope.reasonLabel = reasonData.label;

          $scope.reasons = lodash.map(reasonData.reasons, function(reason) {
            return {label: reason};
          });

          $scope.ok = function () {
            $scope.submitted = true;
            if ($scope.selection.$valid) {
              modal.close({
                reason: $scope.reason.label,
                comments: $scope.comments,
                booking: booking.id,
                type: reasonData.reportType
              });
            }
          };

          $scope.cancel = function() {
            modal.dismiss();
          };

        },
        templateUrl: 'views/modals/select-reason.html'
      });

      return modal.result;
    }


  });
