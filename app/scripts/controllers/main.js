'use strict';

/**
 * @ngdoc function
 * @name trafficApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the trafficApp
 */
angular.module('trafficApp')
  .controller('MainCtrl', function ($scope, login, usSpinnerService, $http, $timeout, urls, $modal, ngToast) {

    // Will hold any login messages
    $scope.messages = {};

    $scope.login = function () {

      // Clear any messages
      $scope.messages = {};

      login.login($scope.user).then(function () {
          setLoginStatus();
        },
        function (messages) {
          // Unsuccessful login
          $scope.messages = messages;
        });

    };

    // Show a loading spinner while there are pending HTTP requests
    $scope.$watch(function isLoading() {
      return $http.pendingRequests.length > 0;
    }, function(isLoading) {
      if (isLoading) {
        $timeout(function() {
          usSpinnerService.spin('loading');
        });
      } else {
        $timeout(function() {
          usSpinnerService.stop('loading');
        });
      }
    });

    var serverAbsentModalVisible = false;
    // If the server is absent, show a dialog to that effect
    $scope.$on('event:http-server-absent', function() {
      if ( ! serverAbsentModalVisible ) {
        serverAbsentModalVisible = true;
        $modal.open({
          templateUrl: 'views/modals/server-absent.html',
          controller: function ($scope, $modalInstance, serverUrl) {
            $scope.serverUrl = serverUrl;
            $scope.close = $modalInstance.close;
          },
          resolve: {
            serverUrl: function () {
              return urls.checkStatus;
            }
          }
        }).result.finally(function() {
            serverAbsentModalVisible = false;
            tryServer();
          });
      }
    });

    /**
     * Check to see if server is present.
     */
    function tryServer() {
      return $http.get(urls.checkStatus);
    }


    // We could also listen for this event:
    //$scope.$on('event:http-server-present', function() {
    // ...
    //});


    /**
     * Set a scope variable on this controller with the value of the login service's loggedIn status.
     */
    function setLoginStatus() {
      login.checkLogin().then(function (loggedIn) {
        $scope.loggedIn = loggedIn;
        // negation explicitly defined, to avoid having to use {{ ! loggedOut }} in template, which will initially
        // evaluate to `true`.
        $scope.loggedOut = ! loggedIn;
        if (loggedIn) {
          // Once logged in, 'forget' the credentials
          $scope.user = {username: '', password: ''};
        }
      });
    }

    setLoginStatus();

    /**
     * Listen for login required, and show modal login if current ui-router state is not the login page.
     */
    $scope.$on('event:auth-loginRequired', setLoginStatus);

    $scope.$on('event:auth-forbidden', setLoginStatus);

    /**
     * Listen for login confirmation
     */
    $scope.$on('event:auth-loginConfirmed', setLoginStatus);

    $scope.$on('event:http-server-error', function(event, errorRejection) {
      ngToast.danger('Server error');
      if (console && 'function' === typeof console.log) {
        console.log(errorRejection);
      }
    });

    /**
     * Logout action
     */
    $scope.logout = function() {
      login.logout();
      setLoginStatus();
    };

  });
