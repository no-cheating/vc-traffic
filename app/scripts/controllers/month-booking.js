'use strict';

/**
 * @ngdoc function
 * @name trafficApp.controller:MonthBookingCtrl
 * @description
 * # MonthBookingCtrl
 * Monthly overview, displaying total numbers.
 */
angular.module('trafficApp')
  .controller('MonthBookingCtrl', function ($scope, bookings, $filter, $state, $stateParams, lodash) {

    /**
     * Default to today's month
     */
    $scope.month = $stateParams.month || $filter('date')(new Date(), 'yyyy-MM');

    $scope.$watch('month', function(month) {
      $state.go('month', {
        month: month
      });
    });

    $scope.hours = lodash.range(0, 24);

    $scope.allBookings = bookings;

    $scope.$watch('visibleBookings', function(bookings) {

      var todayString = formatDate(new Date());

        // Whenever the visible bookings change, compute their arrangement into days then hours.
        // (This is not efficient!)
        $scope.days = lodash.map(getDateRange(), function (date) {
          var daysBookings = lodash.filter(bookings, {startDate: date});
          return {
            date: date,
            isToday: date == todayString,
            count: daysBookings.length,
            hours: lodash.map($scope.hours, function (hour) {
              return {
                hour: hour,
                count: lodash.filter(daysBookings, {scheduledHour: hour}).length
              };
            })
          };
        });

        $scope.highestDayCount = lodash.max(lodash.pluck($scope.days, 'count'));

        $scope.highestHourCount = lodash.reduce($scope.days, function (max, day) {
          var highestHourCount = lodash.max(lodash.pluck(day.hours, 'count'));
          return highestHourCount < max ? max : highestHourCount;
        });

    });

    /**
     * Navigate to the day view for the given day.
     * @param date
     */
    $scope.goto = function(date) {
      $state.go('day', {
        day: formatDate(date)
      });
    };

    /**
    * Get the Dates, as strings, weakly after $scope.month which are in the same month as it.
    * @returns {Array[string]}
    */
    function getDateRange() {
      var date = new Date($scope.month),
        startMonth = date.getMonth(),
        range = [];
      while (date.getMonth() === startMonth) {
        range.push(formatDate(date));
        date.setDate(date.getDate() + 1);
      }
      return range;
    }

    /**
     * A string containing the year, month and day of a date.
     * @param date
     * @returns {*}
     */
    function formatDate(date) {
      return $filter('date')(new Date(date), 'yyyy-MM-dd');
    }

  });
