'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:vcBookingFilters
 * @description
 * # vcBookingFilters
 */
angular.module('trafficApp')
  .directive('vcBookingFilters', function (lodash, $filter) {
    return {
      require: '?ngModel',
      scope: {
        all: '=all',
        filtered: '=filtered'
      },
      templateUrl: 'views/directives/booking-filters.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

        /**
         * All of the special request options, with their labels.
         * @type {Array}
         */
        var AVAILABLE_OPTIONS = [
          {
            key: 'goodsLift',
            cssClass: 'special-option-goods-lift',
            label: 'Goods lift'
          },
          {
            key: 'porter',
            cssClass: 'special-option-porter',
            label: 'Porter'
          },
          {
            key: 'forklift',
            cssClass: 'special-option-forklift',
            label: 'Forklift'
          }
        ];

        /**
         * The function called when data or filters change.
         * @type {Function}
         */
        var onChange = lodash.flow(filterBookings, setFilterOptions);

        scope.$watch('all', onChange);

        scope.$watch('filters', onChange, true);

        scope.$watch('textFilter', onChange);

        /**
         * Clear all filters.
         */
        scope.clearFilters = function() {
          scope.filters = {};
          scope.textFilter = '';
        };

        scope.clearFilters();

        /**
         * Filters the bookings.  Sets and returns $scope.bookings.
         * @returns {Array} The filtered bookings
         */
        function filterBookings() {

          var filtered = $filter('filter')(scope.all, scope.filters, function(actual, expected) {
            return (expected) ? actual == expected : true;
          });

          // 'Fuzzy' search
          var patterns = lodash.map(scope.textFilter.split(' '), function(word) {
            return new RegExp(word.trim(), 'i');
          });

          filtered = lodash.filter(filtered, function(booking) {
            return lodash.every(patterns, function(regexp) {
              return lodash.some(lodash.values(booking), function(value) {
                return String(value).match(regexp);
              });
            });
          });

          // Only display the 'special request' options which are represented in the filtered bookings.
          scope.specialRequestOptions = lodash.filter(AVAILABLE_OPTIONS, function(option) {
            return lodash.some(filtered, option.key);
          });

          return scope.filtered = filtered;
        }

        scope.toggle = function(filterKey) {
          if (scope.filters[filterKey]) {
            delete scope.filters[filterKey];
          } else {
            scope.filters[filterKey] = true;
          }
        }

        /**
         * Sets the scope's filter options, using the given array of bookings.
         * Ensure that the for each filter, if an option is selected, that option occurs in the list.
         */
        function setFilterOptions() {
          scope.options = lodash.reduce(['standZone', 'eventName', 'companyName'], function (options, filterKey) {
            var availableOptions = lodash.pluck(scope.filtered, filterKey);
              availableOptions.push(scope.filters && scope.filters[filterKey]);
            options[filterKey] = lodash.sortBy(lodash.filter(lodash.uniq(availableOptions)), function(value) {
              return String(value).toUpperCase();
            });
            return options;
          }, {});
        }

      }
    };
  });
