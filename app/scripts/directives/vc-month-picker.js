'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:monthPicker
 * @description
 * # monthPicker
 */
angular.module('trafficApp')
  .directive('vcMonthPicker', function (Booking, $filter) {
    return {
      templateUrl: 'views/directives/month-picker.html',
      restrict: 'E',
      scope: {
        month: '=month',
        date: '=date'
      },
      link: function postLink(scope, element, attrs) {

        // If the month-string changes, the underlying date gets set to a UTC midnight at the beginning of that month.
        scope.$watch('month', function dayChanged() {
          scope.date = Booking.localiseUTCDate(scope.month);
        });

        scope.toggleDatepicker = function() {
          scope.datepickerVisible = ! scope.datepickerVisible;
        };

        scope.label = attrs.label || '';

        // If the underlying date changes, the day-string gets updated.
        scope.$watch('date', function dateChanged(date) {
          var dateString;
          if (date) {
            dateString = $filter('date')(date, 'yyyy-MM');
            if (dateString != scope.month) {
              scope.month = dateString;
            }
          }
        });
      }
    };
  });
