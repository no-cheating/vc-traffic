'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:vcBookingStatusses
 * @description
 * # vcBookingStatusses
 */
angular.module('trafficApp')
  .directive('vcBookingStatusses', function(lodash, TrafficStatus, venueSettings) {
    return {
      templateUrl: 'views/directives/booking-statusses.html',
      restrict: 'E',
      scope: {
        bookings: '=bookings',
        filtered: '=filtered'
      },
      link: function postLink(scope, element, attrs) {

        var categories = [{
          filter: function(booking) {
            return booking.status !== TrafficStatus.STATUS_DECLINED && TrafficStatus !== TrafficStatus.STATUS_CANCELLED;
          },
          label: 'All'
        }];
        // Only display these two statusses if the venue requires it.
        if (venueSettings.bookings.approvalRequired) {
          categories = categories.concat([
            bookingStatusFilter(TrafficStatus.STATUS_UNSCHEDULED), {
              filter: function(booking) {
                return booking.status === TrafficStatus.STATUS_DECLINED || TrafficStatus === TrafficStatus.STATUS_CANCELLED;
              },
              label: 'Declined'
            }
          ]);
        }


        scope.categories = categories.concat([
          bookingStatusFilter(TrafficStatus.STATUS_SCHEDULED),
          bookingStatusFilter(TrafficStatus.STATUS_CHECKED_IN),
          bookingStatusFilter(TrafficStatus.STATUS_CHECKED_OUT),
          bookingStatusFilter(TrafficStatus.STATUS_REFUSED),
          bookingStatusFilter(TrafficStatus.STATUS_CANCELLED)
        ]);

        //HACK: Shuffles the order of the catergories to match design.

        Array.prototype.move = function(from, to) {
          this.splice(to, 0, this.splice(from, 1)[0]);
        };

        var arrLen = scope.categories.length;

        if (arrLen > 6) {
          scope.categories.move(2, arrLen);
          scope.categories.move(1, (arrLen - 3));
        } else if (arrLen == 6) {
          scope.categories.move((arrLen - 1), arrLen);
        }


        scope.$watch('bookings', filterItems, true);

        scope.$watch('selected', assignFilteredItems, true);

        /**
         * The selected category
         * @type {{category: {object}}}
         */
        scope.selected = {
          category: scope.categories[0]
        };

        /**
         * A filter for the given status ID.
         * @param {int} statusID
         * @returns {{filter: {status: int}, label: string}}
         */
        function bookingStatusFilter(statusID) {
          return {
            filter: {
              status: statusID
            },
            label: TrafficStatus.statusText(statusID)
          };
        }

        /**
         * Apply each filter to the input bookings.
         * This size of this gets displayed, and the result is stored in the category in case the filter is selected.
         */
        function filterItems() {
          lodash.forEach(scope.categories, function(category) {
            category.filteredItems = lodash.filter(scope.bookings, category.filter);
          });
        }

        /**
         * Assign the selected category's filteredItems as the scope's filtered items.
         * @returns {Array}
         */
        function assignFilteredItems() {

          return scope.filtered = scope.selected && scope.selected.category && scope.selected.category.filteredItems;

        }


      }



    };
  });
