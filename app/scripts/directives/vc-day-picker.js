'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:vcDaypicker
 * @description
 * # vcDaypicker
 * Daypicker.  Mediates between datepicker UTC date(time)s on the one hand, and date strings on the other.
 */
angular.module('trafficApp')
  .directive('vcDayPicker', function ($state, $stateParams, $filter, Booking) {
    return {
      templateUrl: 'views/directives/day-picker.html',
      restrict: 'E',
      scope: {
        'day': '=day',
        'date': '=date'
      },
      link: function postLink(scope, element, attrs) {

        // If the day-string changes, the underlying date gets set to a UTC midnight for that day.
        scope.$watch('day', function dayChanged() {
          scope.date = Booking.localiseUTCDate(scope.day);
        });

        scope.toggleDatepicker = function() {
          scope.datepickerVisible = ! scope.datepickerVisible;
        }

        scope.label = attrs.label || '';

        // If the underlying date changes, the day-string gets updated.
        scope.$watch('date', function dateChanged(date) {
          var dateString;
          if (date) {
            dateString = $filter('date')(date, 'yyyy-MM-dd');
            if (dateString != scope.day) {
              scope.day = dateString;
            }
          }
        });

      }
    };
  });
