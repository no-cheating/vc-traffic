'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:vcDayRangePicker
 * @description
 * # vcDayRangePicker
 * For picking timeless date ranges.
 */
angular.module('trafficApp')
  .directive('vcDayRangePicker', function () {
    return {
      templateUrl: 'views/directives/day-range-picker.html',
      scope: {
        'dayStart': '=start',
        'dayEnd': '=end',
        'onSubmit': '&onSubmit'
      },
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

        scope.submit = function() {
          if (isValid()) {
            scope.$eval(scope.onSubmit);
          }
        };

        scope.$watch('dateStart', validate);
        scope.$watch('dateEnd', validate);

        /**
         * Validates range.
         */
        function validate() {
          scope.invalid = ! isValid();
        }

        /**
         * Ensures start date not after end date.
         * @returns {boolean}
         */
        function isValid() {

          if ( ! scope.dateStart )
            return false;

          if ( ! scope.dateEnd )
            return false;

          if ( scope.dateStart > scope.dateEnd ) {
            return false;
          }

          return true;

        }

      }
    };
  });
