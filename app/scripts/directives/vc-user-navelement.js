'use strict';

/**
 * @ngdoc directive
 * @name trafficApp.directive:vcUserNavelement
 * @description
 * # vc nav bar user element
 */

angular.module('trafficApp')
  .directive('vcUserNavelement', function() {
    return {
      templateUrl: 'views/directives/vc-user-navelement.html',
      restrict: 'E',
      scope: {
      }
    };
  });
