# traffic

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.0.

## Setup of development environment

	cd vc-traffic
	gem install compass
	npm install
	bower install

## Build & development

Run `grunt serve:dev` for preview, running against the dev server.

`grunt build:<target>` will build (minifying etc) for the specified target.

## Testing

Running `grunt test` will run the unit tests with karma.
